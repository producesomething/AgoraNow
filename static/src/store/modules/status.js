// Status module
export default {
	state: {
		data: []
	},
	mutations: {
		pushStatus(state, _data) {
			state.data.push(_data);
		},
		pushStatusArray(state, _dataArray) {
			for(let i in _dataArray) {
				this.data.push(_dataArray[i])
			}
		}
	},
	getters: {
		// Order by created_at
		getStatusesOrderByCreatedAt: (state, direction) => {
			let resultList = Object.assign({}, state.data);
			resultList.sort((a, b) => {
				return direction * (a.createdAt - b.createdAt);
			});
			return resultList;
		},

		// Filter by Author name
		getStatusesByAuthor: (state, author) => {
			let resulList = state.data.filter(_data => {
				return _data.author == author;
			});
			return resultList;
		},

		// Filter by status_id
		getStatusesByAuthor: (state, status_id) => {
			let resulList = state.data.filter(_data => {
				return _data.author == status_id;
			});
			return resultList;
		}
	}
}

