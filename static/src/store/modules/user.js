// User module
export default {
	state: {
		username: "",
		sessionID: "",
	},
	mutations: {
		setUsername: (state, username) => {
			state.username = username;
		},
		setSessionID: (state, sessionID) => {
			state.sessionID = sessionID;
		},
		setLogins: (state, username, sessionID) => {
			this.setUserID(username);
			this.setSessionID(sessionID)
		}
	},
	getters: {
		getUsername: state => {
			return state.username;
		},
		getSessionID: state => {
			return state.sessionID;
		}
	}
}

