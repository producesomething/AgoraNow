import Vuex from "vuex"
import user from "./modules/user.js";
import status from "./modules/status.js";

export default new Vuex.Store({
	modules:{
		user,
		status
	}
})
